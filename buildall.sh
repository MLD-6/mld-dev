#!/bin/sh
# build the whole project

umask 0002

cd ${0%/*}

git pull
(cd meta-mld && git pull)

export USER=${USER:-$(whoami)}

for machine in x86 x86-qemu rpi2 rpi3 rpi4 rpi5 rock-pi-4 tinker-board tinker-board-s odroidn2l-hardkernel odroidn2plus-hardkernel; do
  echo "------------------------------------"
  echo "Build $machine image"
  kas build mld6-$machine.yml
  echo "------------------------------------"
  echo "Build $machine packages"
  kas shell mld6-$machine.yml -c "bitbake -k --runall build packagegroup-all"
  kas shell mld6-$machine.yml -c "bitbake package-index"
done
